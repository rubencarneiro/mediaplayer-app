# Lomiri Media Player App

Media Player App is the media player for Lomiri capable of playing a variety of
audio and video formats both on mobile devices and desktop computers. It is the
default application for playing videos on Ubuntu Touch.

## i18n: Translating Lomiri Media Player App into your Language

You can easily contribute to the localization of this project (i.e. the
translation into your language) by visiting (and signing up with) the Hosted
Weblate service: https://hosted.weblate.org/projects/lomiri/mediaplayer-app

The localization platform of this project is sponsored by Hosted Weblate via
their free hosting plan for Libre and Open Source Projects.
